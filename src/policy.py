#!/usr/bin/env python2.7

import numpy as np
from numpy.linalg import inv
np.random.seed(42) # Make sure it's reproducable

alpha    = 0.26
NR_USER_FEATURES    = 6    # dimension of user features
NR_ARTICLE_FEATURES = 6    # dimension of article features

def inv_func(matrix):
    return inv(matrix.get())
def trans_func(matrix):
    return np.transpose(matrix.get())

class Cache():
    def defaultfunc(self):
        return self.constant
    def __init__(self, recalcfunc=defaultfunc, *ass):
        self.args = ass
        self.calculator = recalcfunc
        self.value = None
        self.dependers = []

    def invalidate(self):
        for c in self.dependers:
            c.invalidate()
        self.value = None

    def depends_on(self, other):
        other.dependers.append(self)

    def set(self, val):
        for c in self.dependers:
            c.invalidate()
        self.value = val

    def get(self):
        if self.value is None: # Lazy recalculation
            self.set(self.calculator(*self.args))
        return self.value


class Recommender():
    def __init__(self, articles):
        self.article = dict()

        self.last_article = None
        self.last_user = None


    def get_article(self, article_id):
        if article_id not in self.article:
            self.article[article_id] = Article()
        return self.article[article_id]

    def recommend(self, user, article_ids):
        best_article_id = None
        best_ucb = None

        for article_id in article_ids:
            a = self.get_article(article_id)
            ucb_a = a.get_ucb(user, self)
            if ucb_a > best_ucb: # anything > None == True
                best_ucb = ucb_a
                self.last_article = a
                chosen = article_id

        self.last_user = user
        return chosen

    def update(self, reward):
        if reward == -1:
            return

        a = self.last_article
        u = self.last_user

        z = u.z.get()

        M = a.M.get()
        M += np.outer(z, z)
        a.M.set(M)

        b = a.b.get()
        b += np.multiply(z, reward)
        a.b.set(b)


class Article(object):
    def __init__(self):
        self.M = Cache()
        self.M.set(np.identity(NR_ARTICLE_FEATURES))

        self.M_inv = Cache(inv_func, self.M)
        self.M_inv.depends_on(self.M)

        self.b = Cache()
        self.b.set(np.zeros(NR_ARTICLE_FEATURES))

        def w_func(M_inv, b):
            return M_inv.get().dot(b.get())
        self.w = Cache(w_func, self.M_inv, self.b)
        self.w.depends_on(self.M_inv)
        self.w.depends_on(self.b)

        self.w_t = Cache(trans_func, self.w)
        self.w_t.depends_on(self.w)

    def get_ucb(self, user, recommender):
        w_t = self.w_t.get()
        z = user.z.get()
        M_inv = self.M_inv.get()

        ucb_1 = w_t.dot(z)
        ucb_2_1 = z.dot(M_inv).dot(z)
        ucb = ucb_1 + alpha * np.sqrt(ucb_2_1)

        return ucb


class User(object):
    def __init__(self, features, recommender):
        self.z = Cache()
        self.z.set(features)

def set_articles(articles):
    global recommender
    recommender = Recommender(articles)


def reccomend(time, user_features, articles):
    global recommender
    user_features = np.array(user_features)
    user = User(user_features, recommender)
    return recommender.recommend(user, articles)

def update(reward):
    global recommender
    recommender.update(reward)
