import sys
import policy
import time

INTERVAL = 1000

if __name__ == "__main__":

    if (len(sys.argv) != 3):
        raise Exception("Usage: ./evaluator.py articles_file log_file")

    with file(sys.argv[1]) as inf:
        articles = dict()
        for line in inf:
            features = line.strip().split(" ")
            key = int(features[0])
            article = [float(x) for x in features[1:]]
            articles[key] = article
        policy.set_articles(articles)

    score = 0
    total_evaluated = 0
    n_lines = 0

    counter = 1
    last_time = time.clock()
    time_sum = 0
    time_count = 0

    with file(sys.argv[2]) as inf:
        for line in inf:
            if counter % INTERVAL == 0:
                now = time.clock()
                elapsed = (now - last_time)*1000
                time_sum += elapsed
                time_count += 1
                rolling = time_sum / time_count

                last_time = now
                print INTERVAL, "took", "avg:", ("%.0f" % rolling), " ", "real:", ("%.0f" % elapsed), "ms"

            n_lines += 1
            logline = line.strip().split()
            chosen = int(logline.pop(7))
            reward = int(logline.pop(7))
            log_time = int(logline[0])
            user_features = [float(x) for x in logline[1:7]]
            articles = [int(x) for x in logline[7:]]

            calculated = policy.reccomend(log_time, user_features, articles)

            if calculated == chosen:
                policy.update(reward)
                score += reward
                total_evaluated += 1
            else:
                policy.update(-1)
            counter += 1

        print "Evaluated %d/%d lines." % (total_evaluated, n_lines)
        print "CTR=%f" % (float(score) / total_evaluated)
